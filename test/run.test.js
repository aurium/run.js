"use strict";

var run = require('..');
var assert = require('assert');

describe('Run module', function() {
    xit = ()=> null

    xit('run a simple command', function() {
        return run('ls').then(function(output){
            assert.equal(
                'node_modules package.json package-lock.json run.js test',
                output.split('\n').join(' ').replace(/\s+/, ' ').trim()
            );
        });
    });

    xit('run a command with a param', function() {
        return run('echo', 'ok').then(function(output){
            assert.equal('ok', output.trim());
        });
    });

    xit('pipe a command', function() {
        return run('cat', 'package.json')
        .pipe('grep', 'main')
        .then(function(output){
            assert.equal(
                '"main": "run.js",',
                output.replace(/\s+/, ' ').trim()
            );
        });
    });

    xit('pipe a command two times', function() {
        return run('echo', '-e', 'C\nB\nC\nB\nB\nA')
        .pipe('sort')
        .pipe('uniq')
        .then(function(output){
            assert.equal(
                'A B C',
                output.split('\n').join(' ').trim()
            );
        });
    });

    function testErr(attrs) {
        return function (err) {
            console.log('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', err.message)
            for (var k in attrs) {
                var v = attrs[k];
                if (v.constructor == RegExp) assert(v.test(err[k]));
                else assert.equal(JSON.stringify(v), JSON.stringify(err[k]));
            }
        }
    }

    xit('rejects a fail command', function() {
        return run('ls', '/not/valid/path')
        .catch(testErr({
            constructor: run.errorType.Failed,
            code: "PROC_FAILED",
            cmd: "ls",
            params: ["/not/valid/path"],
            statusCode:2,
            output: /ls: não foi possível acessar/
        }))
        .catch((e)=>console.log(333333333,e))
    });

    it('rejects a fail command before pipe', function() {
        return run('ls', '/not/valid/path')
        .pipe('cat')
        .catch(testErr({
            constructor: run.errorType.Failed,
            code: "PROC_FAILED",
            cmd: "ls",
            params: ["/not/valid/path"],
            statusCode: 2,
            output: /ls: não foi possível acessar/
        }))
    });

    xit('rejects a fail command after pipe', function() {
        return run('echo', 'lalala')
        .pipe('grep', 'nonono')
        .catch(testErr({
            constructor: run.errorType.Failed,
            code: "PROC_FAILED",
            cmd: "grep",
            params: ["nonono"],
            statusCode: 1,
            output: ""
        }))
    });

});
