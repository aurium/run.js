"use strict";

const {spawn} = require('child_process');

const timeout = (secs, callback)=> setTimeout(callback, secs*1000);

/*
 * Merge objects in a **new** object.
 */
const merge = (...objs)=> Object.assign({}, ...objs);

class FailedProcError extends Error {
    constructor(cmd, params, statusCode, output) {
        super(`${cmd} exit with status code ${statusCode}.`);
        this.code = 'PROC_FAILED';
        this.cmd = cmd;
        this.params = params;
        this.statusCode = statusCode;
        this.output = output;
        Error.captureStackTrace(this, FailedProcError);
    }
}

class KilledProcError extends Error {
    constructor(cmd, params, signal, output) {
        super(`${cmd} killed with signal ${signal}.`);
        this.code = 'PROC_KILLED';
        this.cmd = cmd;
        this.params = params;
        this.signal = signal;
        this.output = output;
        Error.captureStackTrace(this, KilledProcError);
    }
}

class PromiseWithProcPipe extends Promise {
    constructor(resolve, reject) {
        super(resolve, reject);
    }
    pipe(cmd, ...params) {
        var secondCmd = run(cmd, ...params);
        this.proc.stdout.on('data', (data)=> secondCmd.proc.stdin.write(data));
        this.proc.on('close', (code) => {
            //if (code !== 0)
            secondCmd.proc.stdin.end();
        });
        return secondCmd.then((output)=> this.then(()=> output ));
    }
}

function setupProc(proc) {
    proc.output = '';
    proc.__kill = proc.kill;
    proc.kill = function(signal='SIGTERM') {
        if (!proc.killed) {
            proc.stdin.destroy(null);
            proc.stdout.destroy(null);
            proc.stderr.destroy(null);
            proc.__kill(signal);
        }
    };
    proc.stderr.on('data', (data)=> proc.output += data.toString());
    proc.stdout.on('data', (data)=> proc.output += data.toString());
}

/*
 * Run a command.
 * The first argument is the command itself.
 * The follow arguments are its parammeters.
 * An optional configuration object as last argument, with this keys:
 * * env: set a new env to the command, as an object like {VAR: 'value'}.
 * * addEnv: add variables to the current env.
 * * cwd: set the command work dir. (defaults to project root)
 * * timeout: [seconds in int] if set, it will kill the command.
 * Returns a promisse, with a `proc` attribute that refferer to a spawned child process.
 */
function run(cmd, ...params) {
    var proc, conf = {}, lastParamIndex = params.length-1;
    if (typeof(params[lastParamIndex])=='object') conf = params.pop();
    var spawnConf = {
        cwd: conf.cwd || process.cwd(),
        env: merge(conf.env || process.env)
    };
    try {
        spawnConf.env = merge(spawnConf.env, conf.addEnv);
        proc = spawn(cmd, params, spawnConf);
        setupProc(proc);
    } catch(err) {
        return Promise.reject(123);
    }
    var runPromise = new Promise(function (resolve, reject) {
        if (conf.timeout) timeout(conf.timeout, proc.kill);
        proc.on('close', function (code, signal) {
            if (code && code != 0) return reject(new FailedProcError(cmd, params, code, proc.output));
            if (signal) return reject(new KilledProcError(cmd, params, signal, proc.output));
            resolve(proc.output);
        });
    });

//    runPromise.pipe = function(cmd, ...params) {
//        var secondCmd = run(cmd, ...params);
//        proc.stdout.on('data', (data)=> secondCmd.proc.stdin.write(data));
//        proc.on('close', (code) => {
//            //if (code !== 0)
//            secondCmd.proc.stdin.end();
//        });
//        return secondCmd.then((output)=> runPromise.then(()=> output ));
//    };
    withPipe(runPromise, proc);

    runPromise.proc = proc;
    return runPromise;
}

function withPipe(firstPromisse, proc) {
    firstPromisse.pipe = function(cmd, ...params) {
        var secondCmd = run(cmd, ...params);
        proc.stdout.on('data', (data)=> secondCmd.proc.stdin.write(data));
        proc.on('close', (code) => {
            //if (code !== 0)
            secondCmd.proc.stdin.end();
        });
        secondCmd.__then = secondCmd.then;
        secondCmd.__catch = secondCmd.catch;
        secondCmd.then = (cb)=> secondCmd.__then(
            (output)=> firstPromisse.then(()=> output )
        ).then(cb);
        secondCmd.catch = (cb)=> secondCmd.__then(
            (output)=> firstPromisse.then(()=> output )
        ).catch(cb);
        return secondCmd;
    };
}


run.errorType = {
  Failed: FailedProcError,
  Killed: KilledProcError
};
module.exports = run;
